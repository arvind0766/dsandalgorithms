package hackerRank;

import java.util.Scanner;

public class TimeConversion {

	static String timeConversion(String s) {
		// 07:05:45PM
		// not completely solved

		String[] timeArray = s.split(":");

		int hrTime = Integer.parseInt(timeArray[0]);
		int minTime = Integer.parseInt(timeArray[1]);

		int secTime = Integer.parseInt(timeArray[2].substring(0, 2));

		String format = timeArray[2].substring(2, 4);
		int hrToAdd = 0;
		if (format.equals("PM")) {
			hrToAdd = 12;
		}
		hrTime += hrToAdd;
		return getZeroAppended(hrTime) + ":" + getZeroAppended(minTime) + ":" + getZeroAppended(secTime);

	}

	static String getZeroAppended(int number) {
		if (number < 10) {
			return "0" + number;
		} else {
			return String.valueOf(number);
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.next();
		String result = timeConversion(s);
		System.out.println(result);
	}

}
