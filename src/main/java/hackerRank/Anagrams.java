package hackerRank;

import java.util.Scanner;

public class Anagrams {
	// not working with all use cases
	static int anagram(String s) {
		if (s.length() % 2 == 1) {
			return -1;
		}

		String s1 = s.substring(0, s.length() / 2);
		String s2 = s.substring(s.length() / 2, s.length());
		int[] s1Store = new int[26];
		int[] s2Store = new int[26];

		for (int i = 0; i < s1.length(); i++) {
			int charS1 = s1.charAt(i) - 97;
			int charS2 = s2.charAt(i) - 97;
			s2Store[charS2] = s2Store[charS2] + 1;
			s1Store[charS1] = s1Store[charS1] + 1;
		}

		int s1TotalSame = 0, s2Total = 0;

		for (int i = 0; i < 26; i++) {
			if (s2Store[i] > 0 && s2Store[i] > s1Store[i]) {
				s2Total += s2Store[i];
				s1TotalSame += s1Store[i];
			}
		}
		return s2Total - s1TotalSame;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int q = in.nextInt();
		for (int a0 = 0; a0 < q; a0++) {
			String s = in.next();
			int result = anagram(s);
			System.out.println(result);
		}
	}
}
