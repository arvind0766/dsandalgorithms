package hackerRank;

import java.util.Scanner;

public class SuperReducedString {
	static String super_reduced_string(String s) {
		// not working in all cases

		char[] charArray = s.toCharArray();
		String reducedString = String.valueOf(charArray[0]);
		for (int i = 1; i < charArray.length; i++) {
			char currentChar = charArray[i];

			if (reducedString.length() > 0 && currentChar == reducedString.charAt(reducedString.length() - 1)) {
				reducedString = reducedString.substring(0, reducedString.length() - 1);
			} else if (reducedString.length() == 0) {
				reducedString = String.valueOf(currentChar);
			} else {
				reducedString += currentChar;
			}
		}
		return reducedString;

	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.next();
		String result = super_reduced_string(s);
		System.out.println(result);
	}
}
