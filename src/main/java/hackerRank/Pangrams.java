package hackerRank;

import java.util.Scanner;

public class Pangrams {
	public static void main(String args[]) throws Exception {
		Scanner in = new Scanner(System.in);
		String s1 = in.next();
		isStringPangram(s1);

	}

	private static void isStringPangram(String s1) {
		boolean[] letterOccurence = new boolean[26];
		s1 = s1.toLowerCase();
		for (int i = 0; i < s1.length(); i++) {
			char currentChar = s1.charAt(i);

			letterOccurence[currentChar - 97] = true;
		}

		for (int i = 0; i < 26; i++) {
			if (!letterOccurence[i]) {
				System.out.println("not pangram");
				return;
			}
		}
		System.out.println("pangram");
	}
}
