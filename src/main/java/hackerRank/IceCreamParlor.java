package hackerRank;

import java.util.Scanner;

public class IceCreamParlor {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		for (int a0 = 0; a0 < t; a0++) {
			int m = in.nextInt();
			int n = in.nextInt();
			String s = in.nextLine();
			String q = in.nextLine();
			String[] iceCreamList = q.split(" ");
			int[] iceCreams = new int[iceCreamList.length];
			for (int i = 0; i < iceCreamList.length; i++) {
				iceCreams[i] = Integer.parseInt(iceCreamList[i]);
			}
			spendEntirePool(iceCreams, m);
		}

	}

	private static void spendEntirePool(int[] iceCreamList, int m) {
		for (int i = 0; i < iceCreamList.length; i++) {
			for (int j = i + 1; j < iceCreamList.length; j++) {
				if (iceCreamList[i] + iceCreamList[j] == m) {
					System.out.println(i + 1 + " " + String.valueOf(j + 1));
					return;
				}
			}
		}

	}

}
