package hackerRank;

import java.util.Scanner;

public class CamelCase {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.next();
		camelCase(s);
	}

	static void camelCase(String s) {
		char[] charArr = s.toCharArray();
		int counter = 1;
		for (char element : charArr) {
			if (element >= 65 && element <= 90) {
				counter++;
			}
		}
		System.out.print(counter);
	}
}
