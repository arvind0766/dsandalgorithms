package hackerRank;

import java.util.Scanner;

public class DayofTheProgrammer {
	static int dayOfProgrammer = 256;

	static String solve(int year) {
		int[] monthsDay = { 31, 28, 31, 30, 31, 30, 31, 31 };
		int daysTillAugust = 243;

		// Complete this function
		if (year > 1918) {
			if (year % 400 == 0 || year % 4 == 0 && year % 100 != 0) {
				daysTillAugust = 244;
			}
			int septemberDate = dayOfProgrammer - daysTillAugust;
			return String.valueOf(septemberDate) + ".09." + year;
			// gregorian calendar
		} else if (year < 1918) {
			if (year % 4 == 0) {
				daysTillAugust = 244;
			}
			int septemberDate = dayOfProgrammer - daysTillAugust;
			return String.valueOf(septemberDate) + ".09." + year;
		} else {
			return "";

		}
	}

	static String getZeroAppended(int number) {
		if (number < 10) {
			return "0" + number;
		} else {
			return String.valueOf(number);
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int year = in.nextInt();
		String result = solve(year);
		System.out.println(result);
	}
}
