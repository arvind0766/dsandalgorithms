package hackerRank;

import java.util.Scanner;

public class SeparatetheNumbers {
	// incomplete with use cases
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int q = in.nextInt();
		for (int a0 = 0; a0 < q; a0++) {
			String s = in.next();
			isNumberBeautiful(s);
		}
	}

	static void isNumberBeautiful(String s) {

		for (int i = 0; i < s.length() - 1; i++) {
			String firstNumberString = s.substring(0, i + 1);
			Integer firstNumber = Integer.parseInt(firstNumberString);
			String tempString = s.replaceFirst(String.valueOf(firstNumber), "");
			if (checkForwardStrings(tempString, firstNumber)) {
				System.out.println("YES " + firstNumber);
				return;
			}
		}
		System.out.println("NO");
	}

	static boolean checkForwardStrings(String subString, int number) {
		number++;
		String numberInString = String.valueOf(number);

		while (subString.contains(numberInString) && subString.length() > 0) {
			subString = subString.replaceFirst(numberInString, "");
			number++;
			numberInString = String.valueOf(number);
		}

		if (subString.length() == 0) {
			return true;
		} else {
			return false;
		}

	}

}
