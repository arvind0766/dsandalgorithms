package hackerRank;

import java.util.Scanner;

public class FunnyStrings {

	static String funnyString(String s) {
		boolean funny = true;
		for (int i = 0; i < s.length() / 2; i++) {

			int startCharA = s.charAt(i);
			int startCharB = s.charAt(i + 1);
			int endCharZ = s.charAt(s.length() - i - 1);
			int endCharY = s.charAt(s.length() - i - 2);

			if (Math.abs(startCharB - startCharA) != Math.abs(endCharZ - endCharY)) {
				funny = false;
				break;
			}
		}

		if (funny) {
			return "Funny";
		} else {
			return "Not Funny";
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int q = in.nextInt();
		for (int a0 = 0; a0 < q; a0++) {
			String s = in.next();
			String result = funnyString(s);
			System.out.println(result);
		}
	}

}
