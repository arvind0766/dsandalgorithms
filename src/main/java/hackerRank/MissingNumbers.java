package hackerRank;

import java.util.Scanner;

public class MissingNumbers {
	public static void main(String args[]) throws Exception {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		String s = in.nextLine();
		String q = in.nextLine();
		String[] aList = q.split(" ");

		s = in.nextLine();
		q = in.nextLine();
		String[] bList = q.split(" ");

		int[] intArrB = new int[bList.length];
		int[] intArrA = new int[aList.length];

		Integer min = Integer.parseInt(bList[0]);
		intArrA[0] = Integer.parseInt(aList[0]);
		intArrB[0] = min;
		for (int i = 1; i < bList.length; i++) {
			Integer intElement = Integer.parseInt(bList[i]);
			if (intElement < min) {
				min = intElement;
			}
			intArrB[i] = intElement;
			if (i < aList.length) {
				intElement = Integer.parseInt(aList[i]);
				intArrA[i] = intElement;
			}
		}

		int[] diffArr = new int[100];
		for (int i = 0; i < intArrB.length; i++) {
			diffArr[intArrB[i] - min] = diffArr[intArrB[i] - min] + 1;

			if (i < aList.length) {
				diffArr[intArrA[i] - min] = diffArr[intArrA[i] - min] - 1;
			}
		}

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < diffArr.length; i++) {
			if (diffArr[i] > 0) {
				builder.append(i + min).append(" ");
			}
		}
		System.out.println(builder.toString().substring(0, builder.length() - 1));

	}

}
