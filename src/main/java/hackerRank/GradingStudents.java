package hackerRank;

import java.util.Scanner;

public class GradingStudents {

	static int[] solve(int[] grades) {
		int[] roundOffGrades = new int[grades.length];
		for (int i = 0; i < grades.length; i++) {
			if (grades[i] < 38) {
				roundOffGrades[i] = grades[i];
			} else {
				int nextMultipleOfFive = (grades[i] / 5 + 1) * 5;
				if (nextMultipleOfFive - grades[i] < 3) {
					roundOffGrades[i] = nextMultipleOfFive;
				} else {
					roundOffGrades[i] = grades[i];
				}
			}

		}
		return roundOffGrades;

	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] grades = new int[n];
		for (int grades_i = 0; grades_i < n; grades_i++) {
			grades[grades_i] = in.nextInt();
		}
		int[] result = solve(grades);
		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i] + (i != result.length - 1 ? "\n" : ""));
		}
		System.out.println("");

	}
}
