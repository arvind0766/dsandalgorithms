package hackerRank;

import java.util.HashMap;
import java.util.Scanner;

public class TwoCharacters {
	// incomplete code
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int len = in.nextInt();

		String s = in.next();
		@SuppressWarnings("unchecked")
		HashMap<String, Integer> letterCountMap = new HashMap();
		for (int i = 0; i < s.length(); i++) {
			if (letterCountMap.get(s.substring(i, i + 1)) == null) {
				letterCountMap.put(s.substring(i, i + 1), 1);
			} else {
				letterCountMap.put(s.substring(i, i + 1), letterCountMap.get(s.substring(i, i + 1)) + 1);
			}
		}

		int maxLength = 0;
		if (letterCountMap.keySet().size() == 2 && isPalindrome(s)) {
			maxLength = s.length();
		} else {
			String[] keyArr = (String[]) letterCountMap.keySet().toArray();
			for (int i = 0; i < keyArr.length - 2; i++) {
				for (int j = i + 1; j < keyArr.length; j++) {
					String temp = s.replace(keyArr[i], "");
				}
			}
		}
	}

	public static boolean isPalindrome(String s) {
		for (int i = 0; i <= s.length() / 2; i++) {
			if (!(s.charAt(i) == s.charAt(s.length() - i - 1))) {
				return false;
			}
		}
		return true;
	}
}
