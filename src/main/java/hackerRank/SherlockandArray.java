package hackerRank;

import java.util.Scanner;

public class SherlockandArray {

	static String solve(int[] a, int sum) {
		int leftSum = a[0];
		int rightSum = sum - leftSum;
		int i = 1;
		for (; i < a.length; i++) {
			int midElement = a[i];
			rightSum -= midElement;

			if (leftSum == rightSum) {
				return "YES";
			} else {
				leftSum += midElement;
			}
		}
		return "NO";
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		for (int a0 = 0; a0 < T; a0++) {
			int n = in.nextInt();
			int[] a = new int[n];
			int sum = 0;
			for (int a_i = 0; a_i < n; a_i++) {
				a[a_i] = in.nextInt();
				sum += a[a_i];
			}
			String result = solve(a, sum);
			System.out.println(result);
		}
	}
}
