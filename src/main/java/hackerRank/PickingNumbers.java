package hackerRank;

import java.util.Scanner;

public class PickingNumbers {
	// not working properly
	// 100
	// 4 97 5 97 97 4 97 4 97 97 97 97 4 4 5 5 97 5 97 99 4 97 5 97 97 97 5 5 97
	// 4 5 97 97 5 97 4 97 5 4 4 97 5 5 5 4 97 97 4 97 5 4 4 97 97 97 5 5 97 4
	// 97 97 5 4 97 97 4 97 97 97 5 4 4 97 4 4 97 5 97 97 97 97 4 97 5 97 5 4 97
	// 4 5 97 97 5 97 5 97 5 97 97 97

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] a = new int[n];
		for (int a_i = 0; a_i < n; a_i++) {
			a[a_i] = in.nextInt();
		}
		System.out.println(pickNumber(a));
	}

	static int pickNumber(int[] arr) {
		int lastNumber = arr[0];
		int counter = 1;
		for (int i = 1; i < arr.length; i++) {
			if (Math.abs(arr[i] - lastNumber) <= 1) {
				counter++;
			}
			lastNumber = arr[i];
		}
		return counter;
	}
}
