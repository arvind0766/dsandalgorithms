package hackerRank;

import java.util.Scanner;

public class HackerRankInString {
	// incomplete solution
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int q = in.nextInt();
		String subString = "hackerrank";
		for (int a0 = 0; a0 < q; a0++) {
			String s = in.next();
			stringMatched(s, subString);
			// your code goes here
		}
	}

	private static void stringMatched(String s, String subString) {
		int subStringStartIndex = 0;
		for (int i = 0; i < s.length(); i++) {
			if (subStringStartIndex == subString.length()) {
				subStringStartIndex = 0;
			}
			if (subString.charAt(subStringStartIndex) == s.charAt(i) && subStringStartIndex < subString.length()) {
				subStringStartIndex++;
			}

		}
		if (subStringStartIndex < subString.length()) {
			System.out.println("NO");
		} else {
			System.out.println("YES");
		}
	}
}
