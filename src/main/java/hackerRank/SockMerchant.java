package hackerRank;

import java.util.HashMap;
import java.util.Scanner;

public class SockMerchant {
	static int sockMerchant(int n, int[] ar) {
		HashMap<Integer, Integer> sockPairs = new HashMap(); // Complete this
																// function

		int socksPair = 0;
		for (int element : ar) {
			if (sockPairs.get(element) == null) {
				sockPairs.put(element, 1);
			} else {
				sockPairs.put(element, sockPairs.get(element) + 1);
			}
			if (sockPairs.get(element).toString().equals("2")) {
				sockPairs.put(element, 0);
				socksPair++;
			}
		}
		return socksPair;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] ar = new int[n];
		for (int ar_i = 0; ar_i < n; ar_i++) {
			ar[ar_i] = in.nextInt();
		}
		int result = sockMerchant(n, ar);
		System.out.println(result);
	}
}
