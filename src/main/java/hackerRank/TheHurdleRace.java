package hackerRank;

import java.util.Scanner;

public class TheHurdleRace {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int k = in.nextInt();
		int[] height = new int[n];
		for (int height_i = 0; height_i < n; height_i++) {
			height[height_i] = in.nextInt();
		}
		System.out.println(getMaxDrinks(height, n, k));
	}

	static int getMaxDrinks(int[] heights, int n, int k) {
		// find maximum height hurdle
		int maxHeight = 0;
		for (int height : heights) {
			if (maxHeight < height) {
				maxHeight = height;
			}
		}
		if (maxHeight <= k) {
			return 0;
		} else {
			return maxHeight - k;
		}
	}
}
