package hackerRank;

import java.util.Scanner;

public class WeightedUniformStrings {

	// not working for all use cases
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.next();
		int n = in.nextInt();
		int[] weightedResult = getWeightedUniformString(s);
		for (int a0 = 0; a0 < n; a0++) {
			int x = in.nextInt();
			isWeigtedString(weightedResult, x);

		}
	}

	static void isWeigtedString(int[] weightArr, int x) {
		boolean found = false;
		for (int i = 0; i < 26; i++) {
			if (weightArr[i] == 0) {
				continue;
			}
			if (x > 1) {
				if (weightArr[i] == x || weightArr[i] % x == 0) {
					found = true;
					System.out.println("Yes");
					break;
				}
			} else {
				if (weightArr[i] == x) {
					found = true;
					System.out.println("Yes");
					break;
				}
			}

		}
		if (!found) {
			System.out.println("No");
		}
	}

	public static int[] getWeightedUniformString(String s1) {
		int[] letterOccurence = new int[26];
		for (int i = 0; i < s1.length(); i++) {
			char currentChar = s1.charAt(i);
			int indexValue = currentChar - 97;
			letterOccurence[indexValue] = letterOccurence[indexValue] + indexValue + 1;
		}
		return letterOccurence;
	}

}
