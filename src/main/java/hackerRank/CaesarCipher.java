package hackerRank;

import java.util.Scanner;

public class CaesarCipher {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();

		String s = in.next();
		int k = in.nextInt();
		encryptString(s, k);
	}

	static void encryptString(String s, int k) {
		char[] sArr = s.toCharArray();
		char[] zArr = new char[sArr.length];
		StringBuilder encryptedString = new StringBuilder();

		int counter = 0;
		for (char element : sArr) {

			if (element == '-') {
				zArr[counter++] = element;
			} else {
				int temp = element;
				k %= 26;
				temp = temp + k;
				if (element < 97) {

					if (temp > 90) {
						temp -= 26;
					}
				} else {
					if (temp > 122) {
						temp -= 26;
					}
				}
				zArr[counter++] = (char) temp;
			}
		}
		for (char element : zArr) {
			System.out.print(element);
		}
		// return zArr.toString();
	}
}
