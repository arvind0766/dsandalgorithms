package hackerRank;

import java.util.Scanner;

public class AppleAndOrange {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int t = in.nextInt();
		int a = in.nextInt();
		int b = in.nextInt();
		int m = in.nextInt();
		int n = in.nextInt();
		int[] apple = new int[m];
		for (int apple_i = 0; apple_i < m; apple_i++) {
			apple[apple_i] = in.nextInt();
		}
		int[] orange = new int[n];
		for (int orange_i = 0; orange_i < n; orange_i++) {
			orange[orange_i] = in.nextInt();
		}

		int appleCount = 0, orangeCount = 0;

		for (int apple_i = 0; apple_i < m; apple_i++) {
			int appleLoc = apple[apple_i];
			int appleLocFromA = a + appleLoc;
			if (appleLocFromA >= s && appleLocFromA <= t) {
				appleCount++;
			}
		}

		for (int orange_i = 0; orange_i < n; orange_i++) {
			int orangeLoc = orange[orange_i];
			int orangeLocFromA = b + orangeLoc;
			if (orangeLocFromA >= s && orangeLocFromA <= t) {
				orangeCount++;
			}
		}
		System.out.println(appleCount);
		System.out.println(orangeCount);

	}

}
