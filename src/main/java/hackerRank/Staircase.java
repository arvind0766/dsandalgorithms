package hackerRank;

import java.util.Scanner;

public class Staircase {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = Integer.parseInt(in.nextLine());

		for (int i = 1; i <= t; i++) {
			printCharNTimes(" ", t - i);
			printCharNTimes("#", i);
			System.out.println();
		}
	}

	private static void printCharNTimes(String string, int n) {
		for (int i = 0; i < n; i++) {
			System.out.print(string);
		}

	}

}
