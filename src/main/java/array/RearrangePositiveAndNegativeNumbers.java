package array;

public class RearrangePositiveAndNegativeNumbers {

	public static void main(String[] args) {
		int[] arr = { -12, 11, -13, -5, 6, -7, 5, -3, -6 };
		rearrange(arr);

	}

	public static void rearrange(int[] arr) {
		int neg = 0, pos = arr.length - 1;
		int temp;
		while (neg != pos) {
			if (arr[neg] >= 0) {
				temp = arr[neg];
				arr[neg] = arr[pos];
				arr[pos] = temp;
				pos--;
			} else {
				neg++;
			}

		}

	}

}
