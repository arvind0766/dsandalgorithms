package array;

public class SearchAnElementInSortedAndRotatedArray {

	public static void main(String[] args) {
		int arr[] = { 9, 1, 2, 3, 4, 5, 7, 8 };
		int position = searchElementInSortedArray(arr, 8, 0, arr.length - 1);
		System.out.println(position);

	}

	public static int searchElementInSortedArray(int[] arr, int f, int l, int r) {
		int mid = l + (r - l) / 2;
		if (l == r) {
			if (arr[l] == f) {
				return l;
			} else {
				return -1;
			}

		} else {
			if (arr[mid] == f) {
				return mid;
			} else {
				if (arr[l] > arr[r]) {
					if (arr[r] < f) {
						return searchElementInSortedArray(arr, f, l, mid - 1);
					} else if (arr[l] > f) {
						return searchElementInSortedArray(arr, f, mid + 1, r);
					}
				} else {
					if (arr[mid] > f) {
						return searchElementInSortedArray(arr, f, l, mid - 1);
					} else {
						return searchElementInSortedArray(arr, f, mid + 1, r);
					}
				}
			}
		}
		return -1;
	}


}
