package array;

public class GivensortedAndRotatedArrayFindIfThereIsAPairWithGivenSum {

	public static void main(String[] args) {
		int arr[] = { 2, 3, 4, 5, 7, 8, 9, 10, 1 };

		System.out.println(findSum(arr, 5));
		// System.out.println(findMaxElementIndexInArray(arr, 0, arr.length - 1));
	}

	public static int findMaxElementIndexInArray(int[] arr, int l, int r) {
		int mid = l + (r - l) / 2;
		if (arr[mid] > arr[mid + 1]) {
			return mid;
		} else {
			if (arr[l] > arr[r]) {
				return findMaxElementIndexInArray(arr, l, mid);
			} else {
				return findMaxElementIndexInArray(arr, mid + 1, r);
			}
		}

	}

	public static boolean findSum(int[] arr, int x) {
		int r = findMaxElementIndexInArray(arr, 0, arr.length - 1);
		int l = (r + 1) % arr.length;

		boolean foundSum = false;
		while (r != l) {
			int sum = arr[l] + arr[r];
			if (sum == x) {
				foundSum = true;
				break;
			} else {
				if (sum > x) {
					r = (r - 1) % (arr.length - 1);
				} else {
					l = (l + 1) % (arr.length - 1);
				}
			}
		}
		return foundSum;
	}

}
