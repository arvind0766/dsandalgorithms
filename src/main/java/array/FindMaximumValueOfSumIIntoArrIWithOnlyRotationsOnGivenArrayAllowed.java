package array;

public class FindMaximumValueOfSumIIntoArrIWithOnlyRotationsOnGivenArrayAllowed {

	public static void main(String[] args) {
		int[] arr = { 1, 20, 2, 10 };
		System.out.print(findMax(arr));
	}

	//Not working
	public static int findMax(int[] arr) {
		int arrSum = 0;
		int n = arr.length;
		int currVal = 0;
		for (int i = 0; i < arr.length; i++) {
			arrSum += arr[i];
			currVal += (i + 1) * arr[i];
		}

		int maxSum = currVal;
		for (int j = 1; j < n; j++) {
			currVal = currVal + arrSum - n * arr[n - j];
			if (currVal > maxSum) {
				maxSum = currVal;
			}
		}
		return maxSum;
	}

}
