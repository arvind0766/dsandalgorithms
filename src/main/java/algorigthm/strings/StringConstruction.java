package algorigthm.strings;

import java.util.Arrays;
import java.util.List;

public class StringConstruction {

    public static int findMaxConsecutiveOnes(List<Integer> nums) {
        int count = 0;
        int maxCount = 0;
        for (int index = 0; index < nums.size(); index++) {
            if (nums.get(index) == 1) {
                count++;
            } else {
                if (maxCount < count) {
                    maxCount = count;
                    count = 0;
                }
            }

        }
        return count;
    }

    public static void main(String[] args) {


        System.out.println(-5 * -5);
    }



    public void duplicateZeros(int[] arr) {
        //[1,   0,      2,      3,      0,      4,      5       0]

        //[8,   4,      5,      0,      0,      0,      0,      7]
        //zc=0  zc=0    zc=0    zc=2    zc=4
        //ci=0  ci=1    ci=2    ci=3    ci=4

        //[1,   0,      2,      3,      0,      4,      5       0]
        //ci+zc=7  [1,   0,      2,      3,      0,      4,      5       4]
        //ci+zc=6  [1,   0,      2,      3,      0,      0,      0       4]

        //ci+zc=4  [1,   0,      2,      2,      3,      0,      0       4]


        //[1,0,0,2,3,0,0]
        int zeroCounter = 0;
        int currentArrIndex = 0;

        while ((currentArrIndex + zeroCounter) < arr.length) {
            if (arr[currentArrIndex] == 0) {
                zeroCounter += 2;
            }
            currentArrIndex++;
        }

        while (currentArrIndex > 0) {
            if (arr[currentArrIndex] == 0) {
                arr[currentArrIndex + zeroCounter--] = 0;
                arr[currentArrIndex + zeroCounter--] = 0;
            } else {
                arr[currentArrIndex + zeroCounter--] = arr[currentArrIndex];
            }
            currentArrIndex--;
        }
    }

}
