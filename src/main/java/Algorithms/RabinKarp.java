package Algorithms;

public class RabinKarp {
	public static void main(String[] args) {
		// Scanner in = new Scanner(System.in);
		// String s = in.next();
		// String result = "";
		// System.out.println(result);
		isPatternMatched("12341234556", "12345");
	}

	public static boolean isPatternMatched(String t, String p) {
		int n = t.length();
		int m = p.length();

		int patternHash = 0;
		int textHash = 0;
		char[] patternArr = p.toCharArray();
		char[] textArr = t.toCharArray();
		for (int i = 0; i < m; i++) {
			patternHash = patternHash * 10 + patternArr[i] - '0';
			textHash = textHash * 10 + textArr[i] - '0';
		}
		boolean patternMatched = false;
		for (int i = 1; i < t.length() - m; i++) {
			if (patternHash == textHash) {
				int patternCounter = 0;
				for (int j = i - 1; j <= i + m - 1 - 1; j++) {
					if (textArr[j] == patternArr[patternCounter++]) {
						patternMatched = true;
					} else {
						patternMatched = false;
						break;
					}

				}
			} else {
				textHash = (int) (textHash * 10 - Math.pow(10, m) * (textArr[i - 1] - '0') + textArr[i + m - 1] - '0');
			}
		}
		return patternMatched;
	}
}
