package dynamicProgramming;

public class RodCut {

    public static void main(String[] args) {
        System.out.println(cutRod(10));
    }

    static int cutRod(int n){

        int[] p =  {0,1,5,8,9,10,17,17,20,24,30};
//        int[] r = new int[p.length];
//        for(int i=0;i<r.length;i++){
//            r[i]=-1;
//        }
        return cutRodIterative(p,n);
    }

    static int cutRodRec(int[] p, int n){
        if(n==0){
            return 0;
        }
        int q = Integer.MIN_VALUE;
        for(int i=1;i<=n;i++){
            int curr=p[i]+cutRodRec(p,n-i);
            if(q<curr){
                q=curr;
            }
        }
        return q;
    }

    static int cutRodRecMemo(int[] p, int[] r, int n) {
        if (r[n] >= 0) {
            return r[n];
        }
        if (n == 0) {
            return 0;
        }
        int q = -1;
        for (int i = 1; i <=n; i++) {
            int currMax = p[i-1] + cutRodRecMemo(p, r, n - i);
            if (q < currMax) {
                q = currMax;
            }
        }
        r[n] = q;
        return r[n];
    }

    static int cutRodIterative(int[] p, int n){
        int[] r  = new int[p.length+1];
        for(int i=1;i<=n;i++){
            int max=-1;
            for(int j=1;j<=i;j++){
                max = Math.max(max,p[j]+r[i-j]);
            }
            r[i]=max;
        }
        return r[n];
    }
}
