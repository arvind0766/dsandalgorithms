package hackerPracticeSession;

public class FizzBuzz {

	public static void main(String[] args) {
		for (int i = 1; i <= 100; i++) {
			String n = String.valueOf(i);
			if (i % 5 == 0 && i % 3 == 0) {
				n = "FizzBuzz";
			} else if (i % 5 == 0) {
				n = "Buzz";
			} else if (i % 3 == 0) {
				n = "Fizz";
			}
			System.out.println(n);
		}
	}

}
