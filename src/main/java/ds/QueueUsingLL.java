package ds;

public class QueueUsingLL {

	SingleNode head;

	public void enqueue(int val) {
		if (head == null) {
			head = new SingleNode();
			head.value = val;
		} else {
			SingleNode newNode = new SingleNode();
			newNode.value = val;
			newNode.next = head;
			head = newNode;
		}

	}

	public int dequeue() {
		if (head == null) {
			return -1;
		}

		SingleNode pointer = head;
		SingleNode nextPointer = head.next, currentPointer = null;
		while (nextPointer != null) {
			currentPointer = nextPointer;
			nextPointer = nextPointer.next;
		}

		// SingleNode temp = head;
		//
		// while (pointer.next != null) {
		// pointer = pointer.next;
		// }
		int deval = currentPointer.value;
		// pointer = temp;
		// head = pointer;
		return deval;
	}

	public boolean isEmpty() {
		return head == null;
	}
}
