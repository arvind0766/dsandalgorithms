package ds;

public class SingleLinkedList<T> {
	Node<T> head;

	public void insertAtFirst(T node) {
		if (isEmpty()) {
			head = new Node<T>(node);
		} else {
			Node<T> newNode = new Node<T>(node);
			newNode.setNext(head);
			head = newNode;
		}

	}

	public int getSize() {
		int counter = 0;
		if (!isEmpty()) {
			Node<T> counterNode, nextNode = head.getNext();
			while (nextNode != null) {
				counter++;
				counterNode = nextNode.getNext();
				nextNode = counterNode;
			}

		}
		return counter;
	}

	/**
	 * insert the node at given index
	 * 
	 * @param node
	 * @param index
	 *            index number(starts with 0)
	 */
	public void insertAt(T node, int index) {
		if (isEmpty()) {
			System.out.println("Linked list is empty");
		} else {

			if (index == 0) {
				insertAtFirst(node);
			} else if (index == getSize()) {
				insertAtLast(node);
			} else if (index < 0 || index > getSize()) {
				System.out.println("Out of range number");
			} else {
				int counter = 0;
				Node<T> previousNode = head, nextNode = head.getNext();
				while (counter <= index) {
					counter++;
					previousNode = nextNode;
					nextNode = nextNode.getNext();
				}
				Node<T> newNode = new Node<T>(node);
				previousNode.setNext(newNode);
				newNode.setNext(nextNode);
			}

		}
	}

	public void insertAtLast(T node) {
		if (isEmpty()) {
			head = new Node<T>(node);
		} else {
			Node<T> currentNode = head, nextNode = head.getNext(), previousNode = head;

			while (nextNode != null) {
				previousNode = nextNode;
				currentNode = nextNode.getNext();
				nextNode = currentNode;
			}

			Node<T> newNode = new Node<T>(node);
			previousNode.setNext(newNode);
		}
	}

	public void removeAtFirst() {
		if (isEmpty()) {
			System.out.println("Linked list is empty");
		} else {
			head = head.getNext();
		}
	}

	public void removeAt(int index) {
		if (isEmpty()) {
			System.out.println("Linked list is empty");
		} else {
			if (index == 0) {
				removeAtFirst();
			} else if (index == getSize()) {
				removeAtLast();
			} else if (index < 0 || index > getSize()) {
				System.out.println("Out of range number");
			} else {
				int counter = 0;
				Node<T> previous = head, next = head.getNext();
				while (counter < index) {
					counter++;
					previous = next;
					next = next.getNext();
				}
				previous.setNext(next.getNext());
			}

		}
	}

	public void removeAtLast() {
		if (isEmpty()) {
			System.out.println("Linked list is empty");
		} else {
			Node<T> current = head, next = head.getNext(), previous = head;

			while (next != null) {
				previous = next;
				current = next.getNext();
				next = current;
			}
			previous.setNext(null);
		}
	}

	public void printAllNodes() {
		if (!isEmpty()) {
			Node<T> counter, next = head.getNext();
			System.out.println(head.getT());
			while (next != null) {
				System.out.println(next.getT());
				counter = next.getNext();
				next = counter;
			}
		}
	}

	public boolean isEmpty() {
		return head == null;
	}

	public T getAt(int index) {
		if (index >= 0 && index <= getSize()) {
			// throw new Exception("Index not reachable");

			int counter = 0;
			Node<T> previousNode = head, nextNode = head.getNext();
			while (counter < index) {
				counter++;
				previousNode = nextNode;
				nextNode = nextNode.getNext();
			}
			return previousNode.getT();
		}
		return null;

	}

	public void toCircularList() {

		if (!isEmpty()) {
			Node<T> currentNode = head, nextNode = head.getNext(), previousNode = head;
			while (nextNode != null) {
				previousNode = nextNode;
				currentNode = nextNode.getNext();
				nextNode = currentNode;
			}
			previousNode.setNext(head);
		}
	}

	public static void testOperations() {
		SingleLinkedList<Integer> singleLinkedList = new SingleLinkedList<Integer>();
		singleLinkedList.insertAtLast(1);
		singleLinkedList.insertAtLast(2);
		singleLinkedList.insertAtLast(3);
		singleLinkedList.insertAtLast(4);
		singleLinkedList.insertAtLast(5);
		singleLinkedList.insertAtLast(6);
		singleLinkedList.printAllNodes();
		singleLinkedList.insertAtFirst(0);
		singleLinkedList.printAllNodes();
		singleLinkedList.removeAt(3);
		singleLinkedList.removeAtLast();
		singleLinkedList.removeAtFirst();
		singleLinkedList.printAllNodes();
	}

}
