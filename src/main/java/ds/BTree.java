package ds;

public class BTree {
	public BNode bNode;

	public void insert(int val) {
		if (bNode == null) {
			bNode = new BNode(val);
		} else {
			insert(bNode, val);
		}
	}

	public BNode insert(BNode node, int val) {
		if (node == null) {
			node = new BNode(val);
		} else {
			if (node.left == null) {
				node.left = insert(node.left, val);
			} else if (node.left != null && node.right == null) {
				node.right = insert(node.right, val);
			} else if (node.left != null && node.right != null) {
				node.left = insert(node.left, val);	
			}

		}
		return node;
	}

	public void inorder() {
		inorderTraverse(bNode);
	}

	private void inorderTraverse(BNode node) {
		if (node == null) {
			return;
		}
		if (node.left != null) {
			inorderTraverse(node.left);
		}
		System.out.println(node.val);
		if (node.right != null) {
			inorderTraverse(node.right);
		}
	}
	public void preorder() {
		preOrderTraverse(bNode);
	}

	private void preOrderTraverse(BNode node) {
		if (node == null) {
			return;
		}
		System.out.println(node.val);
		if (node.left != null) {
			preOrderTraverse(node.left);
		}
		
		if (node.right != null) {
			preOrderTraverse(node.right);
		}
		
	}

}
