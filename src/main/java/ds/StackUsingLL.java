package ds;

public class StackUsingLL {
	SingleNode singleNode;

	public int pop() {
		if (singleNode == null) {
			return -1;
		}

		int topValue = singleNode.value;
		if (singleNode.next != null) {
			singleNode = singleNode.next;
		} else {
			singleNode = null;
		}
		return topValue;

	}

	public void push(int val) {
		if (singleNode == null) {
			singleNode = new SingleNode();
			singleNode.value = val;
		} else {
			SingleNode newNode = new SingleNode();
			newNode.value = val;
			newNode.next = singleNode;
			singleNode = newNode;
		}

	}

	public boolean isEmpty() {
		return singleNode == null;
	}

}
