package ds;

public class Node<T> {

	T t;
	Node<T> next;

	public T getT() {
		return t;
	}

	public Node(T node) {
		t = node;
		next = null;
	}

	public Node<T> getNext() {
		return next;
	}

	public void setNext(Node<T> next) {
		this.next = next;
	}

}
