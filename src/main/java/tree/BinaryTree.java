package tree;

//import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;

import ds.QueueUsingLL;
import stack.SLLQueue;

public class BinaryTree {
	BNode<Integer> root;

	public BinaryTree(Integer val) {
		root = new BNode<Integer>(val);
	}

	public BNode<Integer> getRoot() {
		return root;
	}

	public void inorderTraverse() {
		inOrder(root);
	}

	private void inOrder(BNode<Integer> node) {
		if (node == null) {
			return;
		}
		if (node.getLeft() != null) {
			inOrder(node.getLeft());
		}
		System.out.println(node.getT());
		if (node.getRight() != null) {
			inOrder(node.getRight());
		}
	}

	public void preOrderTraverse() {
		preOrder(root);
	}

	private void preOrder(BNode<Integer> node) {
		if (node == null) {
			return;
		}
		System.out.println(node.getT());
		if (node.getLeft() != null) {
			preOrder(node.getLeft());
		}

		if (node.getRight() != null) {
			preOrder(node.getRight());
		}
	}

	public void postOrderTraverse() {
		postOrder(root);
	}

	private void postOrder(BNode<Integer> node) {
		if (node == null) {
			return;
		}

		if (node.getLeft() != null) {
			postOrder(node.getLeft());
		}

		if (node.getRight() != null) {
			postOrder(node.getRight());
		}
		System.out.println(node.getT());
	}

	public void levelOrderTraverse() {
		SLLQueue<BNode<Integer>> treeQueue = new SLLQueue<BNode<Integer>>();
		// if (root != null) {
		// treeQueue.enqueue(root.getT());
		// }
		levelOrder(root, treeQueue);

	}

	private void levelOrder(BNode<Integer> node, SLLQueue<BNode<Integer>> treeQueue) {
		if (node == null) {
			return;
		}
		treeQueue.enqueue(node);
		while (!treeQueue.isEmpty()) {
			System.out.println(treeQueue.peek().getT());
			if (treeQueue.peek().getLeft() != null) {
				treeQueue.enqueue(treeQueue.peek().getLeft());
			}
			if (treeQueue.peek().getRight() != null) {
				treeQueue.enqueue(treeQueue.peek().getRight());
			}
			treeQueue.dequeue();

		}
		// if (node.getLeft() != null && node.getRight() != null) {
		// treeQueue.enqueue(node.getLeft().getT());
		// treeQueue.enqueue(node.getRight().getT());
		// levelOrder(node.getLeft(), treeQueue);
		// levelOrder(node.getRight(), treeQueue);
		// } else if (node.getLeft() != null) {
		// treeQueue.enqueue(node.getLeft().getT());
		// levelOrder(node.getLeft(), treeQueue);
		// } else if (node.getRight() != null) {
		// treeQueue.enqueue(node.getRight().getT());
		// levelOrder(node.getRight(), treeQueue);
		// }

	}

	public static void addLeft(BNode<Integer> bNode, Integer t) {
		if (bNode == null) {
			bNode = new BNode<Integer>(t);
		} else {
			bNode.setLeft(new BNode<Integer>(t));
		}
	}

	public static void addRight(BNode<Integer> bNode, Integer t) {
		if (bNode == null) {
			bNode = new BNode<Integer>(t);
		} else {
			bNode.setRight(new BNode<Integer>(t));
		}
	}

	public int getMaxHeight() {
		return getHeightOfNode(root);
	}

	public int getHeightOfNode(BNode<Integer> node) {
		if (node == null) {
			return 0;
		}
		return Max(getHeightOfNode(node.getLeft()) + 1, getHeightOfNode(node.getRight()) + 1);
	}

	private int Max(int height1, int height2) {
		if (height1 > height2) {
			return height1;
		} else {
			return height2;
		}
	}

	/**
	 * search recursively if node exists in tree
	 * 
	 * @param searchNode
	 * @return
	 */
	public boolean ifNodeExists(BNode<Integer> searchNode) {
		return exists(root, searchNode);
	}

	private boolean exists(BNode<Integer> currentNode, BNode<Integer> searchNode) {
		if (searchNode == null || currentNode == null) {
			return false;
		}
		if (searchNode.getT() == currentNode.getT()) {
			return true;
		}
		boolean exists = exists(currentNode.getLeft(), searchNode);
		if (!exists) {
			exists = exists(currentNode.getRight(), searchNode);
		}
		return exists;
	}

	public int getSize() {
		return getSizeOfNode(root);

	}

	private int getSizeOfNode(BNode<Integer> current) {
		if (current == null) {
			return 0;
		}
		return getSizeOfNode(current.getLeft()) + 1 + getSizeOfNode(current.getRight());
	}

	public int getSizeNonRecursively() {
		SLLQueue<BNode<Integer>> queue = new SLLQueue<BNode<Integer>>();
		queue.enqueue(root);
		int counter = 0;

		while (!queue.isEmpty()) {

			BNode<Integer> dequeuNode = queue.dequeue();

			if (dequeuNode != null) {
				counter++;
				queue.enqueue(dequeuNode.getLeft());
				queue.enqueue(dequeuNode.getRight());
			}
		}
		return counter;
	}

	public void printAllAtLevel(int level) {
		SLLQueue<BNode<Integer>> queue = new SLLQueue<BNode<Integer>>();
		enqueueNodeAtLevel(root, 0, level, queue);
		while (!queue.isEmpty()) {
			System.out.println(queue.dequeue().getT());
		}
	}

	private void enqueueNodeAtLevel(BNode<Integer> node, int currentLevel, int requiredLevel,
			SLLQueue<BNode<Integer>> queue) {

		if (node == null) {
			return;
		}
		if (currentLevel == requiredLevel) {
			queue.enqueue(node);
			return;
		}
		enqueueNodeAtLevel(node.getLeft(), currentLevel + 1, requiredLevel, queue);
		enqueueNodeAtLevel(node.getRight(), currentLevel + 1, requiredLevel, queue);
	}

	public void printLevelOrderInReverse() {
		int maxLevel = getMaxHeight() - 1;

		while (maxLevel > 0) {
			printAllAtLevel(maxLevel);
			maxLevel--;
		}

	}

	public void printAllLeaves() {
		SLLQueue<BNode<Integer>> queue = new SLLQueue<BNode<Integer>>();
		queue.enqueue(root);

		while (!queue.isEmpty()) {

			BNode<Integer> dequeuNode = queue.dequeue();

			if (dequeuNode != null) {
				queue.enqueue(dequeuNode.getLeft());
				queue.enqueue(dequeuNode.getRight());

				if (dequeuNode.getLeft() == null && dequeuNode.getRight() == null) {
					System.out.println(dequeuNode.getT());
				}
			}

		}
	}

	public void printAllAncestor(BNode<Integer> node) {
		findElement(root, node);
	}

	public boolean findElement(BNode<Integer> currentNode, BNode<Integer> searchNode) {
		if (currentNode == null) {
			return false;
		}

		if (currentNode.getT() == searchNode.getT()) {
			System.out.println(currentNode.getT());
			return true;
		}

		if (findElement(currentNode.getLeft(), searchNode) || findElement(currentNode.getRight(), searchNode)) {
			System.out.println(currentNode.getT());
			return true;
		}
		return false;
	}

	public static void testOperations() {
		BinaryTree binaryTree = new BinaryTree(1);

		BinaryTree.addLeft(binaryTree.getRoot(), 2);
		BinaryTree.addRight(binaryTree.getRoot(), 6);
		BinaryTree.addLeft(binaryTree.getRoot().getLeft(), 3);

		BinaryTree.addRight(binaryTree.getRoot().getLeft(), 7);
		BinaryTree.addLeft(binaryTree.getRoot().getLeft().getLeft(), 4);
		BinaryTree.addLeft(binaryTree.getRoot().getLeft().getLeft().getLeft(), 5);

		// binaryTree.preOrderTraverse();
		// binaryTree.inorderTraverse();
		// binaryTree.postOrderTraverse();

		// binaryTree.levelOrderTraverse();
		 BNode<Integer> searchNode = new BNode<Integer>(5);
		// System.out.println(binaryTree.ifNodeExists(searchNode));
		// System.out.print(binaryTree.getSizeNonRecursively());
		// binaryTree.printAllAtLevel(1);
		// binaryTree.printLevelOrderInReverse();
		// binaryTree.printAllLeaves();
		binaryTree.printAllAncestor(searchNode);
	}

}
