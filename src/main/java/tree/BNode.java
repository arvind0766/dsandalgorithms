package tree;

public class BNode<T> {
	T t;
	BNode<T> left, right;

	public T getT() {
		return t;
	}

	public void setT(T t) {
		this.t = t;
	}

	public BNode<T> getLeft() {
		return left;
	}

	public void setLeft(BNode<T> left) {
		this.left = left;
	}

	public BNode<T> getRight() {
		return right;
	}

	public void setRight(BNode<T> right) {
		this.right = right;
	}

	public BNode(T node) {
		t = node;
		left = null;
		right = null;
	}

}
