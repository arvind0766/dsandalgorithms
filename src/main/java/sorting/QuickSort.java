package sorting;

public class QuickSort {

	public static void sort(int[] array, int start, int end) {
		if (start < end) {
			int middle = QuickSort.getPartitionIndex(array, start, end);
			sort(array, start, middle - 1);
			sort(array, middle + 1, end);
		} else {
			return;
		}

	}

	private static int getPartitionIndex(int[] arr, int start, int end) {
		int pivot = arr[end];
		int partitionIndex = start - 1;
		int temp;
		for (int index = start; index < end; index++) {
			if (arr[index] < pivot) {
				partitionIndex++;
				temp = arr[partitionIndex];
				arr[partitionIndex] = arr[index];
				arr[index] = temp;
			}
		}
		temp = arr[partitionIndex + 1];
		arr[partitionIndex + 1] = arr[end];
		arr[end] = temp;
		return partitionIndex + 1;
	}
}
