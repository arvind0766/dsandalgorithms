package sorting;

public class MergeSort {

	public static void sort(int[] arr, int l, int r) {

		if (l < r) {

			int m = l + (r - l) / 2;
			sort(arr, l, m);
			sort(arr, m + 1, r);
			merge(arr, l, m, r);
		}
	}

	static int[] oddNumbers(int l, int r) {
		// List<Integer> myList = new ArrayList<Integer>();
		int firstElement, lastElement;
		if (l % 2 == 1) {
			// first element is odd number
			firstElement = l;
		} else {
			firstElement = l + 1;
		}
		int[] asdda = new int[2];
		return asdda;
		//
		// while (firstElement <= lastElement) {
		// myList.add(firstElement);
		// firstElement += 2;
		// }
		// int[] oddNumbers = myList.toArray(new int[myList.size()]);
		// return oddNumber;

	}

	private static void merge(int[] arr, int l, int m, int r) {

		int[] arrL = new int[m - l];
		int[] arrR = new int[r - l];

		for (int i = l; i < m; i++) {
			arrL[i] = arr[i];
		}
		for (int j = m; j < r; j++) {
			arrR[j] = arr[j];
		}

		int lc = l, rc = m;
		// for (int i = l; i < r; i++) {
		// if (arrL[lc] < arrR[rc]) {
		// arr[i] = arrL[lc];
		// lc++;
		// } else {
		// arr[i] = arrL[rc];
		// rc++;
		// }
		// }

		int i = 0;
		while (lc < m && rc < r) {
			if (arrL[lc] < arrR[rc]) {
				arr[i] = arrL[lc];
				lc++;
			} else {
				arr[i] = arrL[rc];
				rc++;
			}
			i++;
		}

		while (lc < m) {
			arr[i] = arrL[lc];
			lc++;
			i++;
		}

		while (rc < r) {
			arr[i] = arrR[rc];
			rc++;
			i++;
		}
	}

}
