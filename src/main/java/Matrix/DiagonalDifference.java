package Matrix;

import java.util.Scanner;

public class DiagonalDifference {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = Integer.parseInt(in.nextLine());
		int[] arr = new int[t];
		int[][] matrix = new int[t][t];

		for (int i = 0; i < t; i++) {
			String readLine = in.nextLine();
			String[] weights = readLine.split(" ");
			for (int j = 0; j < t; j++) {
				matrix[i][j] = Integer.parseInt(weights[j]);
			}
		}
		findDiagonalDiff(matrix, t - 1);
		// System.out.println("enter number of elements");
		// SolutionCaller.printArray(traverseInSpiral(matrix));
		// System.out.println("Enter all weights of the block separated by
		// spaces and then press enter:");

	}

	public static void findDiagonalDiff(int[][] matrix, int n) {
		int diaglToR = 0, diagRtoL = 0;
		int k = 0;

		while (k <= n) {
			diaglToR += matrix[k][k];
			diagRtoL += matrix[k][n - k];
			k++;
		}
		System.out.println(Math.abs(diagRtoL - diaglToR));

	}
}
