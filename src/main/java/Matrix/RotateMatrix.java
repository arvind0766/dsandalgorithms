package Matrix;

public class RotateMatrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] mat = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 } };
		rotateMatrix(mat, 4);
		System.out.println();
		rotateMatrix(mat, 4);
		System.out.println();
		rotateMatrix(mat, 4);
		System.out.println();
		rotateMatrix(mat, 4);
	}

	public static void rotateMatrix(int[][] matrix, int n) {
		// swap elements and swap entire row for a single column

		for (int i = 0; i < n; i++) {
			for (int j = i; j < n; j++) {
				int temp = matrix[i][j];
				matrix[i][j] = matrix[j][i];
				matrix[j][i] = temp;
			}
		}

		for (int i = 0; i < n / 2; i++) {

			for (int j = 0; j < n; j++) {
				int temp = matrix[j][i];
				matrix[j][i] = matrix[j][n - i - 1];
				matrix[j][n - i - 1] = temp;
			}

		}

		for (int i = 0; i < n; i++) {

			for (int j = 0; j < n; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}

		// for (int i = n - 1; i >= 1; i--) {
		//
		// // outer to inner element traversal
		// // row starts from 4-3-1 i.e. 0, column starts from 3
		// int j = n - i - 1;
		// int k = n - i - 1;
		// int[] temp = matrix[j];
		//
		// //min to i
		// while (j <= i) {
		// // 3,0
		// matrix[k][j] = matrix[n - k + j][k];
		//
		// matrix[]
		// j++;
		// }
		// matrix[j - 1][n - i + j - 1] = temp;
		//
		// }

	}

}
