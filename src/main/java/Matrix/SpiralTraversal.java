package Matrix;

import java.util.Scanner;

//import SolutionCaller;

public class SpiralTraversal {

	public static void traverse() {

		int t = 0;
		Scanner in = new Scanner(System.in);
		t = Integer.parseInt(in.nextLine());
		int[] arr = new int[t];
		int[][] matrix = new int[4][4];
		for (int k = 0; k < t; k++) {
			for (int i = 0; i < 4; i++) {
				String readLine = in.nextLine();
				String[] weights = readLine.split(" ");
				for (int j = 0; j < 4; j++) {
					matrix[i][j] = Integer.parseInt(weights[j]);
				}
			}

			// System.out.println("enter number of elements");
			// SolutionCaller.printArray(traverseInSpiral(matrix));
//			SolutionCaller.printArray(traverseInside(matrix));
			// System.out.println("Enter all weights of the block separated by
			// spaces and then press enter:");

		}
		in.close();
		//
		// Scanner s = new Scanner(System.in);
		// //
		// // System.out.println("enter number of elements");
		// //
		// int t = s.nextInt();

		//
		// for (int testCase = 0; testCase < t; testCase++) {
		// for (int i = 0; i < 4; i++) {
		// String[] row = s.nextLine().split(" ");
		// matrix[i][0] = Integer.parseInt(row[0]);
		// matrix[i][1] = Integer.parseInt(row[1]);
		// matrix[i][2] = Integer.parseInt(row[2]);
		// matrix[i][3] = Integer.parseInt(row[3]);
		// }
		// }

	}

	public static int[] traverseInside(int[][] matrix) {
		int[] arr = new int[16];

		int rowCounter = 0, columnCounter = 0;
		int maxCounter = 3;
		int direction = 0, previousDirection = -1;
		int counterController = 0;
		int i = 0;
		for (; maxCounter > 0;) {
			i++;
			if (counterController == maxCounter) {
				// change direction
				direction = (direction + 1) % 4;
				// maxCounter--;
			}

			if (direction == 0) {

				// left to right
				arr[i] = matrix[rowCounter][columnCounter];
				columnCounter++;
				counterController = columnCounter;

			} else if (direction == 1) {
				// top to bottom
				arr[i] = matrix[rowCounter][columnCounter];
				rowCounter++;
				counterController = rowCounter;
			} else if (direction == 2) {
				// right to left
				arr[i] = matrix[rowCounter][columnCounter];
				columnCounter--;
				counterController = columnCounter;
			} else {
				// bottom to top
				arr[i] = matrix[rowCounter][columnCounter];
				rowCounter--;
				counterController = rowCounter;
			}
		}
		return arr;
	}

	public static int[] traverseInSpiral(int[][] matrix) {

		int[] arr = new int[16];

		int rowCounter = -1, columnCounter = 0;

		boolean leftToRight = false;

		for (int index = 0; index < arr.length; index++) {

			if (columnCounter == 0 || columnCounter == 3) {
				rowCounter++;
				arr[index] = matrix[rowCounter][columnCounter];
				leftToRight = !leftToRight;
				index++;

			}
			if (leftToRight) {
				columnCounter++;
			} else {
				columnCounter--;
			}
			arr[index] = matrix[rowCounter][columnCounter];

		}
		return arr;
	}

}
