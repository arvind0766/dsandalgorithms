package stack;

import ds.SingleLinkedList;

public class SLLStack<T> {
	private SingleLinkedList<T> list;

	public boolean isEmpty() {
		return list.isEmpty();
	}

	public void push(T node) {
		list.insertAtFirst(node);
	}

	public T pop() {
		if (!isEmpty()) {
			T top = list.getAt(0);
			list.removeAtFirst();
			return top;
		} else {
//			System.out.println("Stack is empty");
			return null;
		}

	}

	public SLLStack() {
//		list = new SingleLinkedList<>();
	}

	public T peek() {
		if (!isEmpty()) {
			return list.getAt(0);
		} else {
			return list.getAt(-1);
		}
	}

	public static void testOperations() {
		SLLStack<Integer> stack = new SLLStack<Integer>();
		stack.push(1);
		stack.push(2);
		System.out.println(stack.peek());

		stack.push(3);
		stack.push(4);
		stack.push(5);
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
	}

}
