package stack;

//import java.sql.SQLXML;

import ds.SingleLinkedList;

public class SLLQueue<T> {
	private SingleLinkedList<T> list;

	public SLLQueue() {
		list = new SingleLinkedList<T>();
	}

	public void enqueue(T node) {
		list.insertAtLast(node);
	}

	public T dequeue() {
		if (!isEmpty()) {
			T front = list.getAt(0);
			list.removeAtFirst();
			return front;
		}
		return null;
	}

	public boolean isEmpty() {
		return list.isEmpty();
	}

	public T peek() {
		if (!isEmpty()) {
			return list.getAt(0);
		}
		return null;
	}
	
	public void printAllNodes() {
		list.printAllNodes();
	}

	public static void testOperations() {
		SLLQueue<Integer> sllQueue = new SLLQueue<Integer>();
		sllQueue.enqueue(1);
		sllQueue.enqueue(2);
		sllQueue.enqueue(3);
		System.out.println(sllQueue.dequeue());
		System.out.println(sllQueue.peek());
		System.out.println(sllQueue.dequeue());
		System.out.println(sllQueue.dequeue());
		System.out.println(sllQueue.isEmpty());
		System.out.println(sllQueue.dequeue());
		System.out.println(sllQueue.isEmpty());
	}
}
